<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('search', function() {
    return App\GlobalSearch::search('*')->raw();
});
Route::get('search/{keyword}', function($keyword) {
    return App\GlobalSearch::search($keyword)->raw();
    //return App\GlobalSearch::search($keyword)->paginate(10);
});

Route::resource('post', 'PostController');
