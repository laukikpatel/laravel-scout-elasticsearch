<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('post.index') }}" method="GET" class="form" style="margin-top: 18px;">
                <input name="q" placeholder="Search..." value="{{ request('q') }}" class="form-control" autofocus>
            </form>
        </div>
    </div>
</div>

<div class="album text-muted">
    <div class="container">
        <div class="row">
            @forelse($posts as $post)
                <div class="card">
                    {{--<img alt="{{ $post->title }}" style="width:356px; display: block;" src="{{ $post->img_url }}">--}}
                    <p class="card-title">{{ $post->title }}</p>
                    <p class="card-text">{{ $post->content }}</p>
                </div>
            @empty
                No post found
            @endforelse
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div style="margin-top: 18px;">
                {{ $posts->links('vendor.pagination.simple-bootstrap-4') }}
            </div>
        </div>
    </div>
</div>

<style>
    .album {
        min-height: 50rem;
        padding-top: 3rem;
        padding-bottom: 3rem;
        background-color: #f7f7f7;
    }

    .card {
        float: left;
        width: 33.333%;
        padding: .75rem;
        margin-bottom: 2rem;
        border: 0;
    }

    .card > img {
        margin-bottom: .75rem;
    }

    .card-text {
        font-size: 85%;
    }

    .card-title {
        font-weight: bold;
    }
</style>