<?php

namespace App;

use ScoutElastic\Searchable;
use Illuminate\Database\Eloquent\Model;

class GlobalSearch extends Model
{
    use Searchable;

    protected $indexConfigurator = GlobalGarnerIndexConfigurator::class;

    protected $searchRules = [
        MySearchRule::class
    ];

    protected $mapping = [
        //
    ];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'brands,affiliates';
    }
}