<?php

namespace App;

use ScoutElastic\Searchable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Searchable;

    protected $indexConfigurator = GlobalGarnerIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    protected $mapping = [
        //
    ];
}