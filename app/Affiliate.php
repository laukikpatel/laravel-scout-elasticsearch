<?php

namespace App;

use Carbon\Carbon;
use ScoutElastic\Searchable;
use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model
{
    use Searchable;

    protected $table = 'affiliate';

    protected $primaryKey = 'affiliate_id';

    protected $indexConfigurator = GlobalGarnerIndexConfigurator::class;

    protected $searchRules = [
        MySearchRule::class
    ];

    protected $mapping = [
        //
    ];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'affiliates';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        //$array = $this->toArray();
        return [
            'id' => $this->affiliate_id,
            'title' => $this->name,
            'img' => $this->logo,
            'url' => $this->link,
            'type' => 'affiliate',
            'location' => [],
            'created_at' => Carbon::now()->toDateTimeString()
        ];
    }
}