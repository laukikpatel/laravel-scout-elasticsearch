<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Laravel\Scout\Searchable;
use ScoutElastic\Searchable;

class Post extends Model
{
    use Searchable;

    protected $indexConfigurator = PostsIndexConfigurator::class;

    protected $fillable = ['title', 'content', 'img_url'];

    protected $searchRules = [
        MySearchRule::class
    ];
}
