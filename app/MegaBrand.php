<?php

namespace App;

use Carbon\Carbon;
use ScoutElastic\Searchable;
use Illuminate\Database\Eloquent\Model;

class MegaBrand extends Model
{
    use Searchable;

    protected $table = 'brands';

    protected $primaryKey = 'brand_id';

    protected $indexConfigurator = GlobalGarnerIndexConfigurator::class;

    protected $searchRules = [
        MySearchRule::class
    ];

    protected $mapping = [
        //
    ];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'brands';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        //$array = $this->toArray();
        return [
            'id' => $this->brand_id,
            'title' => $this->brand_name,
            'img' => $this->image,
            'url' => $this->website,
            'type' => 'brand',
            'location' => [],
            'created_at' => Carbon::now()->toDateTimeString()
        ];
    }
}