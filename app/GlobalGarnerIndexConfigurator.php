<?php

namespace App;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class GlobalGarnerIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $settings = [
        //
    ];

    protected $defaultMapping = [
        'properties' => [
            'id' => [
                'type' => 'integer',
            ],
            'title' => [
                'type' => 'string',
            ],
            'img' => [
                'type' => 'string',
            ],
            'url' => [
                'type' => 'string',
            ],
            'type' => [
                'type' => 'string', //affiliate, brand, product, service, vendor
                //'index' => 'not_analyzed'
            ],
            'location' => [
                'type' => 'geo_point', //product, service, vendor, affiliate, brand
                //'index' => 'not_analyzed'
            ],
            'created_at' => [
                'type' => 'date', //product, service, vendor, affiliate, brand
                'format' => 'yyyy-MM-dd HH:mm:ss'
            ]
        ]
    ];
}