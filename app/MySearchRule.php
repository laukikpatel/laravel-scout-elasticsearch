<?php

namespace App;

use ScoutElastic\SearchRule;

class MySearchRule extends SearchRule
{
    public function buildQueryPayload()
    {
        return [
            'must' => [
                'match' => [
                    'title' => $this->builder->query
                ]
            ]
        ];
    }
}