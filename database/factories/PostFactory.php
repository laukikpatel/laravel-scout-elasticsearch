<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {

    $images_category = ['abstract', 'animals', 'business', 'cats', 'city', 'food', 'nightlife', 'fashion', 'people', 'nature', 'sports', 'technics', 'transport'];
    return [
        'title' => $faker->sentence(3),
        'content' => $faker->text(),
        'img_url' => "http://lorempixel.com/356/280/" . array_random($images_category),
    ];
});
